/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

import Models.Words;
import org.apache.commons.lang3.text.WordUtils;


/**
 *
 * 
 * @author ZANDUT
 */
public class Processing
{
    //style 1 : upper case, style 2 : lower case, style 3 : upper in first word
    public String getStyle(Words kata, int style)
    {
        String changed = new String();
        switch (style)
        {
            case 1:
            {
                changed = kata.getKata().toUpperCase();
                break;
            }
            case 2:
            {
                changed = kata.getKata().toLowerCase();
                break;
            }
            case 3:
            {
                
                changed = WordUtils.capitalize(kata.getKata().toLowerCase());
                break;
            }
            
        }
        
        return changed;
    }
    
}
